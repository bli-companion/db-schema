--------------------------------------------------------
--  File created - Wednesday-December-05-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Sequence MEETING_POINT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BLI_SHUTTLE_ADMIN"."MEETING_POINT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SHUTTLE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "BLI_SHUTTLE_ADMIN"."SHUTTLE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table MEETING_POINT
--------------------------------------------------------

  CREATE TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" 
   (	"ID" NUMBER(10,0), 
	"MEETING_POINT_NAME" VARCHAR2(100 CHAR), 
	"PIC_NAME" VARCHAR2(40 CHAR), 
	"PIC_PHONE_NUMBER" VARCHAR2(14 CHAR), 
	"TIME_FROM_BLI" TIMESTAMP (6), 
	"TIME_TO_BLI" TIMESTAMP (6)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table SHUTTLE
--------------------------------------------------------

  CREATE TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" 
   (	"ID" NUMBER(10,0), 
	"CAPACITY" NUMBER(10,0), 
	"NAME" VARCHAR2(40 CHAR), 
	"SIZE_CATEGORY" VARCHAR2(20 CHAR), 
	"MEETING_POINT_ID" NUMBER(10,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
REM INSERTING into BLI_SHUTTLE_ADMIN.MEETING_POINT
SET DEFINE OFF;
REM INSERTING into BLI_SHUTTLE_ADMIN.SHUTTLE
SET DEFINE OFF;
--------------------------------------------------------
--  Constraints for Table MEETING_POINT
--------------------------------------------------------

  ALTER TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" MODIFY ("TIME_TO_BLI" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" MODIFY ("TIME_FROM_BLI" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" MODIFY ("PIC_PHONE_NUMBER" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" MODIFY ("PIC_NAME" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" MODIFY ("MEETING_POINT_NAME" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."MEETING_POINT" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SHUTTLE
--------------------------------------------------------

  ALTER TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" MODIFY ("MEETING_POINT_ID" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" MODIFY ("SIZE_CATEGORY" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" MODIFY ("CAPACITY" NOT NULL ENABLE);
  ALTER TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table SHUTTLE
--------------------------------------------------------

  ALTER TABLE "BLI_SHUTTLE_ADMIN"."SHUTTLE" ADD CONSTRAINT "FKT83CJ4C5RNMEVGWFO1VTPOV1D" FOREIGN KEY ("MEETING_POINT_ID")
	  REFERENCES "BLI_SHUTTLE_ADMIN"."MEETING_POINT" ("ID") ON DELETE CASCADE ENABLE;
